% B1+ difference maps with cylindrical object for C1.1 with 6 cm wires,
% pocket height = 7 mm, pocket width = 50 mm, pocket spacing = 2 mm,
% with er = 700 and sigma = 0.25 S/m with varying relative permittivity
% for the cylindrical phantom with conductivity fixed to 0.50 S/m

clear all;
close all;

% Adjust as needed
addpath('F:\Active Dielectric Shimming\Scripts\');
directory = 'F:\Active Dielectric Shimming\EM Simulations\08 Phantom Relative Permittivity\P0500\';

%% B1+ magnitude maps - coronal and axial slices
planes = ["coronal", "axial", "coronal", "axial"];
slices = [18, 155, 18, 118];
type = ["magnitude" "magnitude" "difference" "difference"];
display_cask = [true false true false];
display_circle = [false false true false];
Vmin = [0 0 -40 -40];
Vmax = [140 140 40 40];
hsize = [1050 1600 1050 1600];
vsize = [1000 500 1000 500];

xc = [92 93 93 101 167 145];
yc = [128 127 126 148 131 131];
E = [5 10 20 40 60 80];
refs = 4.2e-7*[80 83 90 102 110 113]/100;

display_colorbar = true;
pixel_size = [450/384 5.0 450/384];

if (display_colorbar)
    for n = 1:4
        hsize(n) = hsize(n)+200;
        vsize(n) = vsize(n)-120;
    end
end

for n = 1:4
    f = figure(n);
    f.Position = [50 50 hsize(n) vsize(n)];
    t = tiledlayout(2,3);
    t.TileSpacing = 'none';
    t.Padding = 'normal';

    for k = 1:length(E)
        filename = sprintf('CYL D700 E%d P0500 H7 C1.1 6cm.h5',E(k));
        path = strcat(directory, filename);
        [B1_map, mask] = compute_B1_map(path, planes(n), slices(n));

        filename = sprintf('CYL D700 E%d P0500 H7 unwired.h5',E(k));
        path = strcat(directory, filename);
        [B1_map_unwired, mask] = compute_B1_map(path, planes(n), slices(n));
        
        if (type(n) == "magnitude")
            data = B1_map_unwired;
            data = data/refs(k)*100;
        end
        if (type(n) == "difference")
            data = B1_map-B1_map_unwired;
            data = data/refs(k)*100;
            if (planes(n) == "coronal")
                [p(k), ave(k), ci(k,1:2), SD(k)] = get_stats(100*B1_map/refs(k),100*B1_map_unwired/refs(k),xc(k), yc(k));
            end
        end
        
        plot_B1_map_tile(data, Vmin(n), Vmax(n), mask, type(n), display_cask(n), display_circle(n), xc(k), yc(k), planes(n), pixel_size);
    end
    if (type(n) == "difference" && planes(n) == "coronal")
        error3 = max(abs(ave'-ci(:,1)),abs(ave'-ci(:,2)));
    end
    
    if (display_colorbar == true)
        set(gca, 'FontName', 'Inter');
        cb = colorbar;
        cb.Layout.Tile = 'east';
        cb.FontSize = 16;
        if (type(n) == "magnitude")
            title(cb,'B_{1}^{+} (%)','FontSize',16);
        end
        if (type(n) == "difference")
            title(cb,'\DeltaB_{1}^{+} (%)','FontSize',16);
        end
    end
end

function [data, mask] = compute_B1_map(path, plane, slice)
    % Load slice
    B1 = h5read(path,'/B-Field');
    B1 = 2/pi*abs(B1.z.re+1i*B1.z.im);
    
    % Coordinates
    Nx = size(B1,1);
    Ny = size(B1,2);
    Nz = size(B1,3);
    
    if (plane == "coronal")
        B1 = squeeze(B1(:,slice,:));
        data = B1';
        i = -(Nx-1)/2:(Nx-1)/2;
        j = (slice-1);
        k = -(Nz-1)/2:(Nz-1)/2;
        [i, k] = meshgrid(i,k);
    end
    
    if (plane == "axial")
        B1 = squeeze(B1(:,:,slice));
        data = flipud(B1');
        i = -(Nx-1)/2:(Nx-1)/2;
        j = 0:(Ny-1);
        k = (slice-1)-(Nz-1)/2;
        [i, j] = meshgrid(i,j);
    end
    
    x = i*450/384;
    y = j*5.0;
    z = k*450/384;
    
    % Mask (assuming that cylinder is oriented along y-axis)
    r = sqrt(x.*x+z.*z);
    radius = 180;
    mask = double(r < radius);
    mask = flipud(mask);
end

function plot_B1_map_tile(data, V_min, V_max, mask, type, display_cask, display_circle, xc, yc, plane, pixel_size)

    nexttile;
    imagesc(data,'AlphaData',mask);
    
    set(gca,'color',0.92*[1 1 1]);
    if (type == "magnitude")
        colormap jet;
    end
    if (type == "difference")
        cm = cbrewer('div','RdBu',100,'spline');
        cm(cm<0) = 0;
        cm(cm>1) = 1;
        cm = flipud(cm);
        colormap(cm);
    end
    
    if (plane == "coronal")
        daspect([1.0/pixel_size(1) 1.0/pixel_size(3) 1.0]);
    end
    
    if (plane == "axial")
        daspect([1.0/pixel_size(1) 1.0/pixel_size(2) 1.0]);
    end
    
    caxis([V_min V_max]);
    ax = gca;
    ax.XColor = 'none';
    ax.YColor = 'none';
    set(gcf,'color','w');
    
    if (display_cask)
        % Matlab properties
        hold on;
        xlims = xlim;
        ylims = ylim;

        % Cask/imaging parameters
        FOV = 450;
        N = 384;
        num_pockets = 3;
        pocket_width = 50;
        pocket_separation = 2;

        % Compute reference points/distances
        voxel_size = FOV/N;
        cask_size = num_pockets*pocket_width+(num_pockets+1)*pocket_separation;
        cask_size_px = cask_size/voxel_size;
        x0 = (xlims(1)+xlims(2))*0.5-cask_size_px/2;
        y0 = (ylims(1)+ylims(2))*0.5-cask_size_px/2;
        dx = cask_size_px/num_pockets;
        dy = dx;

        for n = 1:num_pockets+1
            x1 = x0;
            x2 = x0 + dx*num_pockets;
            y1 = y0+(n-1)*dy;
            y2 = y1;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end

        for n = 1:num_pockets+1
            x1 = x0+(n-1)*dx;
            x2 = x1;
            y1 = y0;
            y2 = y0 + dy*num_pockets;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end
    end
    
    if (display_circle)
        % Draw ROI
        circle(xc, yc,10);
    end
end

function [p, ave, ci, SD] = get_stats(B1_C1_5V, B1_C1_0V, ic, jc)
    
    r = 10;
    j = 1:size(B1_C1_5V,1);
    i = 1:size(B1_C1_5V,2);
    k = 1:size(B1_C1_5V,3);
    [I, J, K] = meshgrid(i,j,k);
    d = sqrt((I-ic).*(I-ic)+(J-jc).*(J-jc));
    
    xx = B1_C1_5V(d<r);
    yy = B1_C1_0V(d<r);
    [h, p, ci, stats] = ttest(xx(:),yy(:));
    SD = stats.sd;
    ave = mean(xx(:)-yy(:));
end

function h = circle(x,y,r)
    hold on
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    h = plot(xunit, yunit,'r','LineWidth',1.25);
    hold off
end