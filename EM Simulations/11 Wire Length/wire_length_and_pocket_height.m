close all;
clear all;

L = [6 8 10 12 14 16 18 20];

%H3
M3 = [1.6607538,3.6267133,5.5192213,6.7998815,13.683009,20.046890,19.823528, -20.555851];

%H5
M5 = [7.3215227,20.589777,34.457161,-36.511909,-16.585566,-6.8162341,-5.6872125,-4.1055665];

%H7
M7 = [55.120926,-34.657082,-10.460009,-8.1912432,-6.1619449,-3.0373158,-2.2353683,-2.2214503];

%H10
M10 = [-21.420658,-13.960118,-6.9772763,-5.2261386,-3.3187385,-2.1554558,-1.8254462,-1.5742258];

f = figure(1);
t = tiledlayout(1,1);
t.TileSpacing = 'none';
t.Padding = 'none';
hold on;
f.Position = [50 50 1000 1000];

plot(L,abs(M3),'Color',[0 0.5 1],'LineWidth',1.5);
plot(L,abs(M5),'Color',[0 1 0],'LineWidth',1.5);
plot(L,abs(M7),'Color',[1 1 0],'LineWidth',1.5);
plot(L,abs(M10),'Color',[1 0 0],'LineWidth',1.5);
scatter(L,abs(M3),40,'o','MarkerFaceColor',[0 0.5 1],'MarkerEdgeColor',[0 0.5 1],'LineWidth',0.001);
scatter(L,abs(M5),40,'o','MarkerFaceColor',[0 1 0],'MarkerEdgeColor',[0 1 0],'LineWidth',0.001);
scatter(L,abs(M7),40,'o','MarkerFaceColor',[1 1 0],'MarkerEdgeColor',[1 1 0],'LineWidth',0.001);
scatter(L,abs(M10),40,'o','MarkerFaceColor',[1 0 0],'MarkerEdgeColor',[1 0 0],'LineWidth',0.001);

set(gca, 'FontName', 'Inter');
set(gca,'fontsize',16);
grid(gca,'minor')
grid on;
xlabel('Wire length (cm)');
ylabel('\DeltaB_{1}^{+} (%)');
legend('h = 3 mm','h = 5 mm','h = 7 mm','h = 10 mm');