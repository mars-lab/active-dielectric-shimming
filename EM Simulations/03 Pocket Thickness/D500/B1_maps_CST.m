% B1+ difference maps with cylindrical object for C1.1 with 6 cm wires
% and several pocket heights with er = 500 (dielectrics)

clear all;
close all;

% Change to path with utility scripts
addpath('F:\Active Dielectric Shimming\Scripts\');

% Path to files with CST results (.h5 files)
data_path = 'F:\Active Dielectric Shimming\EM Simulations\03 Pocket Thickness\D500\';

f = figure(1);
f.Position = [50 50 1400 1000];
t = tiledlayout(2,4);
t.TileSpacing = 'none';
t.Padding = 'normal';

% ROI centroids
xc = [145 136 165 122 131 131 131 131 141 98];
yc = [121 128 127 130 132 131 131 131 146 97];

% Pocket heights
H = ["5" "7_5" "10" "12_5" "15" "20" "25" "30"];

% Slice number (from 1 to 21 with 5 mm spacing between slices)
slice = 18;

% Reference B1+ magnitude
ref = 4.2e-7;

for k = 1:length(H)
    filename = sprintf('CYL D500 H%s C1.1 6cm.h5',H(k));
    path = strcat(data_path, filename);
    [B1_map, mask] = compute_B1_map(path, slice, ref);

    filename = sprintf('CYL D500 H%s unwired.h5',H(k));
    path = strcat(data_path, filename);
    [B1_map_unwired, mask] = compute_B1_map(path, slice, ref);

    data = B1_map'-B1_map_unwired';
    is_difference = true;
    display_cask = true;
    plot_B1_map_tile(data, -40, 40, mask, is_difference, display_cask, xc(k), yc(k))
    
    [p(k), ave(k), ci(k,1:2), SD(k)] = get_stats(B1_map',B1_map_unwired',xc(k), yc(k));
end
error = max(abs(ave'-ci(:,1)),abs(ave'-ci(:,2)));

set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'\DeltaB_{1}^{+} (%)','FontSize',16);

function [data, mask] = compute_B1_map(path, slice, ref)    
    B1 = h5read(path,'/B-Field');
    B1 = 2/pi*abs(B1.z.re+1i*B1.z.im)/ref*100;
    B1 = squeeze(B1(:,slice,:));
    Nx = size(B1,2);
    Ny = size(B1,1);
    i = -(Nx-1)/2:(Nx-1)/2;
    j = -(Ny-1)/2:(Ny-1)/2;
    [i, j] = meshgrid(i,j);
    r = sqrt(i.*i+j.*j);
    radius = 150; % mask out pixel outside this radius
    mask = double(r<(radius));
    data = B1.*mask';
end

function plot_B1_map_tile(data, V_min, V_max, mask, is_difference, display_cask, xc, yc)

    nexttile;
    imagesc(data,'AlphaData',mask);
    
    set(gca,'color',0.92*[1 1 1]);
    if (is_difference==1)
        cm = cbrewer('div','RdBu',100,'spline');
        cm(cm<0) = 0;
        cm(cm>1) = 1;
        cm = flipud(cm);
        colormap(cm);
    else
        colormap jet;
    end

    caxis([V_min V_max]);
    daspect([1 1 1]);
    ax = gca;
    ax.XColor = 'none';
    ax.YColor = 'none';
    set(gcf,'color','w');
    
    if (display_cask)
        % Matlab properties
        hold on;
        xlims = xlim;
        ylims = ylim;

        % Cask/imaging parameters
        FOV = 450;
        N = 384;
        num_pockets = 3;
        pocket_width = 50;
        pocket_spacing = 2;

        % Compute reference points/distances
        voxel_size = FOV/N;
        cask_size = num_pockets*pocket_width+(num_pockets+1)*pocket_spacing;
        cask_size_px = cask_size/voxel_size;
        x0 = (xlims(1)+xlims(2))*0.5-cask_size_px/2;
        y0 = (ylims(1)+ylims(2))*0.5-cask_size_px/2;
        dx = cask_size_px/num_pockets;
        dy = dx;

        for n = 1:num_pockets+1
            x1 = x0;
            x2 = x0 + dx*num_pockets;
            y1 = y0+(n-1)*dy;
            y2 = y1;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end

        for n = 1:num_pockets+1
            x1 = x0+(n-1)*dx;
            x2 = x1;
            y1 = y0;
            y2 = y0 + dy*num_pockets;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end
    end
    
    % draw ROI
     circle(xc, yc,10);
end

function [p, ave, ci, SD] = get_stats(B1_C1_5V, B1_C1_0V, ic, jc)
    
    r = 10;
    j = 1:size(B1_C1_5V,1);
    i = 1:size(B1_C1_5V,2);
    k = 1:size(B1_C1_5V,3);
    [I, J, K] = meshgrid(i,j,k);
    d = sqrt((I-ic).*(I-ic)+(J-jc).*(J-jc));
    
    xx = B1_C1_5V(d<r);
    yy = B1_C1_0V(d<r);
    [h, p, ci, stats] = ttest(xx(:),yy(:));
    SD = stats.sd;
    ave = mean(xx(:)-yy(:));
end

function h = circle(x,y,r)
    hold on
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    h = plot(xunit, yunit,'r','LineWidth',1.25);
    hold off
end