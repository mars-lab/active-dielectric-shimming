function [] = display_and_save_map(img, window_level, color_map, cb_label, title2use, scan_number, outputFolderDir, extensionsStr)

dimensions = size(img);

if length(dimensions)>2
    multipleImg = true;
else
    %is a single image
    multipleImg = false;
end

if multipleImg
    % for loop
    for sl=1:dimensions(3)
        %loop over slices
        fh = figure;
        temp_img = img(:,:,sl);
        imagesc(temp_img,window_level);
        colormap(color_map);
        cb = colorbar;
        cb.Label.String = cb_label;

        axis off;
        axis image;
        title(sprintf('%s', title2use), 'Interpreter', 'none');
        fh.WindowState = 'maximized';
        filename = sprintf('map_0%d_%s_sl%d', scan_number, title2use, sl);
        saveMatlabFigure(fh, filename, outputFolderDir, 'none', extensionsStr,600);
        clear filename;
    end
else
    %single image
    fh = figure;
    imagesc(img,window_level);
    colormap(color_map);
    cb = colorbar;
    cb.Label.String = cb_label;

    axis off;
    axis image;
    title(sprintf('%s', title2use), 'Interpreter', 'none');
    fh.WindowState = 'maximized';
    filename = sprintf('map_0%d_%s', scan_number, title2use);
    saveMatlabFigure(fh, filename, outputFolderDir, 'none', extensionsStr,600);
    clear filename;
end
end