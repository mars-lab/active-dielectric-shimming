function [cm] = createColormaps(nrOfPointsColormap)
% Create a structure cm with many different colormaps
% Author: Joao Tourais
% Date: November 2021

fileDir = fileparts(which('createColormaps.m'));

%% CBREWER
% div
cm.Spectral = cbrewer('div','Spectral',nrOfPointsColormap,'pchip'); 
cm.RdYlGn = cbrewer('div','RdYlGn',nrOfPointsColormap,'pchip'); 
cm.RdYlBu = cbrewer('div','RdYlBu',nrOfPointsColormap,'pchip'); 
cm.RdGy = cbrewer('div','RdGy',nrOfPointsColormap,'pchip'); 
cm.RdBu = cbrewer('div','RdBu',nrOfPointsColormap,'pchip'); 
cm.PuOr = cbrewer('div','PuOr',nrOfPointsColormap,'pchip'); 
cm.PRGn = cbrewer('div','PRGn',nrOfPointsColormap,'pchip'); 
cm.PiYG = cbrewer('div','PiYG',nrOfPointsColormap,'pchip'); 
cm.BrBG = cbrewer('div','BrBG',nrOfPointsColormap,'pchip'); 


%alpha = 0.1; 
%cm.Spectral = interp1(nrOfPointsColormap./(1:nrOfPointsColormap),cm.Spectral,linspace(1,nrOfPointsColormap*alpha,nrOfPointsColormap));

% seq
cm.YlOrRd = cbrewer('seq','YlOrRd',nrOfPointsColormap,'pchip');
cm.YlOrBr = cbrewer('seq','YlOrBr',nrOfPointsColormap,'pchip');
cm.YlGnBu = cbrewer('seq', 'YlGnBu', nrOfPointsColormap);
%cm.YlGn = cbrewer('seq', 'YlGn', nrOfPointsColormap);
cm.Reds = cbrewer('seq','Reds',nrOfPointsColormap,'pchip');
%cm.RdPu = cbrewer('seq', 'RdPu', nrOfPointsColormap);
cm.Purples = cbrewer('seq', 'Purples', nrOfPointsColormap);
cm.PuRd = cbrewer('seq','PuRd',nrOfPointsColormap,'pchip');
%cm.PuBuGn = cbrewer('seq', 'PuBuGn', nrOfPointsColormap);
cm.PuBu = cbrewer('seq', 'PuBu', nrOfPointsColormap);
cm.OrRd = cbrewer('seq','OrRd',nrOfPointsColormap,'pchip');
cm.Oranges = cbrewer('seq', 'Oranges', nrOfPointsColormap);
cm.Greys = cbrewer('seq', 'Greys', nrOfPointsColormap);
cm.Greens = cbrewer('seq','Greens',nrOfPointsColormap,'pchip');
cm.GnBu = cbrewer('seq', 'GnBu', nrOfPointsColormap);
%cm.BuPu = cbrewer('seq', 'BuPu', nrOfPointsColormap);
%cm.BuGn = cbrewer('seq', 'BuGn', nrOfPointsColormap);
cm.Blues = cbrewer('seq', 'Blues', nrOfPointsColormap);

% qual
cm.Set3 = cbrewer('qual', 'Set3', nrOfPointsColormap );
%cm.Set2 = cbrewer('qual', 'Set2', nrOfPointsColormap );
%cm.Set1 = cbrewer('qual', 'Set1', nrOfPointsColormap );
%cm.Pastel2 = cbrewer('qual', 'Pastel2', nrOfPointsColormap );
%cm.Pastel1 = cbrewer('qual', 'Pastel1', nrOfPointsColormap );
%cm.Paired = cbrewer('qual', 'Paired', nrOfPointsColormap );
%cm.Dark2 = cbrewer('qual', 'Dark2', nrOfPointsColormap );
%cm.Accent = cbrewer('qual', 'Accent', nrOfPointsColormap );


%% Colorgray
cm.colorgray = colorgray(nrOfPointsColormap);

%% CK map
cm.ckmap = ckmap(nrOfPointsColormap);
cm.ckmapLLCine = ckmapLLCine(nrOfPointsColormap);

%% Chiara's T1rho colormaps
% Colormap used for T1rho
% Blue - Green - Yellow - Orange - Red
load([fileDir '\cmap.mat']); 
cm.cmap_t1rho = cmap;
clear cmap;

% Colormap used for precision, reproducibility etc
% White - Yellow - Orange - Red
load([fileDir '\cmapRed.mat']); 
cm.cmap_red = cmapRed;
clear cmapRed;


end

