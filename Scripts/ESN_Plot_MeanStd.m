% This function plots the mean and standard deviation of a dataset.
% It takes the X values, Y mean values, Y standard deviation values, and a color vector as inputs.

% This MATLAB script defines a function ESN_Plot_MeanStd that generates a plot showing the mean and standard deviation of a dataset. The function takes the X values, Y mean values, Y standard deviation values, and a color vector as inputs.
% The script begins by enabling the "hold on" feature, which allows multiple plots to be overlaid on the same figure.
% Next, the input vectors are converted into column vectors to ensure consistent dimensions.
% Invalid data points containing NaN values are identified using the isnan function. These invalid data points are then removed from all input vectors using logical indexing.
% The lower and upper bounds of the data points are calculated by subtracting and adding the standard deviation from the mean, respectively.
% The filled area is created by combining the X and Y values. The function fill is used to fill the area with a specified color and transparency.
% The mean values are plotted as data points using the plot function, with a specified marker style, size, and color.
% A white outline is added to the plotted data points using the patch function. The outline has a specified line width, transparency, and color.
% Finally, the "hold off" feature is disabled, allowing for future plots to be created on a new figure. The function returns the plot handle for the mean values, which can be used for further customization or referencing the plotted object.

function ax_mean = ESN_Plot_MeanStd(X_Values, Y_Mean, Y_Std, Color_Vec)
    hold on % Enable the "hold on" feature to overlay multiple plots on the same figure.
    
    % Convert the input vectors into column vectors.
    X_Values = X_Values(:);
    Y_Mean = Y_Mean(:);
    Y_Std = Y_Std(:);
    
    % Find any invalid data points that contain NaN values.
    invalid_ind = isnan(X_Values) | isnan(Y_Mean) | isnan(Y_Std);
    
    % Remove the invalid data points from all input vectors.
    X_Values = X_Values(~invalid_ind);
    Y_Mean = Y_Mean(~invalid_ind);
    Y_Std = Y_Std(~invalid_ind);
    
    % Calculate the upper and lower bounds of the data points.
    y_min = Y_Mean - Y_Std;
    y_max = Y_Mean + Y_Std;
    
    % Create the filled area by combining the X and Y values.
    x_fill = [X_Values; X_Values(end:-1:1)];
    y_fill = [y_max; y_min(end:-1:1)];
    
    % Fill the area with a specified color and transparency.
    fill(x_fill, y_fill, 'c', 'FaceColor', Color_Vec, 'FaceAlpha', 0.35, 'LineStyle', 'none');
    
    % Plot the mean values as data points with a specified marker style and color.
    ax_mean = plot(X_Values, Y_Mean, 'o', 'MarkerSize', 8, 'MarkerFaceColor', Color_Vec, 'MarkerEdgeColor', 'k');
    
    % Add a white outline to the plotted data points.
    patch([X_Values; NaN], [Y_Mean; NaN], 'w', 'linewidth', 2, 'EdgeAlpha', 0.8, 'EdgeColor', Color_Vec);
    
    hold off % Disable the "hold on" feature to allow for future plots on a new figure.
end
