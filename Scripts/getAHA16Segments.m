function [myoSegmentsLabels] = getAHA16Segments(myocardialMask, nSegments, insertionPoint)
%GETAHA16SEGMENTS Segment myocardium according to AHA 16 segments
%
%   This function uses poolar coordinates and computes the distance in
%   polar coordinates of every pixel in the image from the insertionPoint
%   with respect to the centerPoint.
%   Polar coordinates are then converted in angles and values are
%   discritized to get final segmentation.
%   
%   INPUTS:
%       - myocardialMask    :   2D binary mask with myocardial segmentation
%       - nSegments         :   nr of segments used to divide the myocardium
%       - insertionPoint    :   complex coordinate of insertion point
%  
%   OUTPUT:
%       - myoSegmentsLabels :   2D mask with segmentation labels
%                               (0=external; [1:6]=segments 1...6)

% Initialize variables 
myoSegmentsLabels = myocardialMask;
maskPolar = complex(zeros(size(myocardialMask)));


% Trnsform myocardial pixel indexes to polar coordinates
indexMyocardium = find(myocardialMask == 1);
coordMyocardium = (1 + floor( (indexMyocardium-1) / size(myocardialMask,1))) + 1i * mod(indexMyocardium, size(myocardialMask,1));

% Create polar coordinates matrix
for i = 1:size(myoSegmentsLabels, 1)
    for j = 1:size(myoSegmentsLabels, 2)
        maskPolar(i, j) = i + 1i*j;
    end
end

% Set insertion point and center point in polar coordinates
if ~exist('insertionPoint', 'var')
    insertionPoint = coordMyocardium(1); 
end
[subMyocardiumX, subMyocardiumY] = ind2sub(size(myocardialMask), indexMyocardium);
centerPoint = mean(subMyocardiumX) + 1i * mean(subMyocardiumY);

%Compute distance from centerPoint in polar coordinates
Z = coordMyocardium - centerPoint; 
refZ = insertionPoint - centerPoint; 
maskPolarC = maskPolar - centerPoint;

% Compute angle of every myocardial pixel wrt centerPoint and insertionPoint
refAngle = mod(atan2(imag(refZ),real(refZ))./pi.*180 + 360, 360);
angles = mod(mod(atan2(imag(Z),real(Z))./pi.*180 + 360, 360) - refAngle + 360, 360);
maskAngles = mod(mod(atan2(imag(maskPolarC),real(maskPolarC))./pi.*180 + 360, 360) - refAngle + 360, 360);

% Segment angle values and get final segments IDs
segmentId = mod(floor(angles / (360 / nSegments)) + 1, nSegments) + 1;
myoSegmentsLabels = mod(floor(maskAngles ./ (360 / nSegments)) + 1, nSegments) + 1;
myoSegmentsLabels = myoSegmentsLabels.*myocardialMask;

end

