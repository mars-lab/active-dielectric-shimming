function DrawBullsEye(hFigure, sliceValues, color, range, BullsEyesInfo, Options)
% DRAWBULLSEYE  Draw Bullseye plot for quantitative cardiac MRI
%
%   Author: Sebastian Weingartner
%   Modified by: Chiara Coletti
%   
%   INPUTS:
%       hfigure         :    figure handle
%       (BullsEyesInfo) :    struct containing summary of data to plot
%       (Options)       :    struct containing plot options   
%   

opengl('software')
% The slice order must be B, M, A for the plots
%sliceValues([1,2,3], :) = sliceValues([3, 2, 1], :);
if ~exist('BullsEyesInfo','var')   
    BullsEyesInfo.SlicesCount = 1:3;
    for nSlice = BullsEyesInfo.SlicesCount
        if nSlice == 3
            BullsEyesInfo.Data(nSlice).SliceValues = sliceValues(nSlice, 1:4);
        else
            BullsEyesInfo.Data(nSlice).SliceValues = sliceValues(nSlice, :);
        end
        BullsEyesInfo.Data(nSlice).SliceMean = mean(BullsEyesInfo.Data(nSlice).SliceValues);
    end        
    BullsEyesInfo.DataRange = range;
    %load 'C:\Users\ccoletti\Documents\MATLAB\T1rho_mapping\cmap.mat'
    BullsEyesInfo.ColorMap = color;
%     globalMean = 0;
%     for nSlice = BullsEyesInfo.SlicesCount
%         globalMean = globalMean + BullsEyesInfo.Data(nSlice).SliceMean;
%     end
%     globalMean = globalMean/BullsEyesInfo.SlicesCount(end);
    globalMean = mean(nonzeros(sliceValues));
    BullsEyesInfo.GlobalMean = globalMean;
    BullsEyesInfo.HealthyValue = 230;
    BullsEyesInfo.TextColor = 'k';
    Options.fillColor = 1; Options.drawEdges = 1; Options.WriteValue = 1; 
    Options.lineWidth = 3; Options.BullseyeTextFontSize = 38; Options.BullseyeTextFontName = 'Bahnschrift';
    Options.SliceAverageTextFontSize = 25;
    Options.ShowSliceAverage = 1; Options.ShowGlobalAverage = 1;
    Options.WriteMeanTitle = 1; Options.TextColor = 'k';
    Options.ShowNormalValue = 0; Options.ShowColorbar = 0;
    f = figure; hFigure = gca; hold on; axis off;    
end

xlim([-1 1]);ylim([-1 1]);
SlicesLabels = ['B' 'M' 'A'];
BullsEyesInfoData = BullsEyesInfo.Data;
NumberOfSlices = length(BullsEyesInfoData);

FullRadius = 0.98;
%axes(hFigure);
rStart = FullRadius ./ (NumberOfSlices+1);
for nSlice=1:NumberOfSlices    
    MaxSegment = length(BullsEyesInfoData(nSlice).SliceValues);
    if nSlice == 3
        MaxSegment = 4;
    end
    for nSegment=1:MaxSegment        
        %axes(hFigure); % some fancy animation :)
        value = BullsEyesInfoData(nSlice).SliceValues(nSegment);
        clr = getClr(value, BullsEyesInfo.DataRange, BullsEyesInfo.ColorMap);
        str = sprintf(getFormatStr(value,1),value);
        if (isnan(value))
            clr = [1 1 1];
        end
        [xData,yData] = DrawOneSegment(clr,nSlice,nSegment,MaxSegment,FullRadius,rStart,Options.fillColor,Options.drawEdges, Options.lineWidth);                    
        zData = mean(xData) + 1i.*mean(yData);        
        ff = 1.05;
        if nSlice==NumberOfSlices, ff=1.1; end
        zData = ff*abs(zData)*exp(1i*angle(zData));        
        if (Options.WriteValue && MaxSegment<24)
            if (~isnan(value))
                WriteStringWithCenterPoint(str,real(zData),imag(zData),'Normal',Options.BullseyeTextFontSize,BullsEyesInfo.TextColor); 
            else
                WriteStringWithCenterPoint('NA',real(zData),imag(zData),'Normal',Options.BullseyeTextFontSize,BullsEyesInfo.TextColor); 
            end
        end
    end   
    
    if NumberOfSlices>3 || ~Options.ShowSliceAverage, continue; end
    
    rect.left   =  FullRadius - 0.5 + 0.1*nSlice;
    rect.bottom = -FullRadius - 0.1 + 0.1*nSlice;
    rect.width  = 0.1+0.1*(3-nSlice+1);
    rect.height = 0.1;
    
    value = BullsEyesInfoData(nSlice).SliceMean; 
    str = ''; clr = 'none';    
    if ~isempty(value)
        clr = getClr(value, BullsEyesInfo.DataRange, BullsEyesInfo.ColorMap);
        str = sprintf(getFormatStr(value,1),value);
    end
    
    if isfield( Options ,'SliceAverageTextFontSize') 
        DrawRectangle(rect,clr);    
        WriteStringWithCenterPoint(str,rect.left+rect.width/2,rect.bottom+rect.height/2,'normal',Options.SliceAverageTextFontSize);    
        rect.left   = rect.left - 0.1; rect.width  = 0.1;
        WriteStringWithCenterPoint(SlicesLabels(nSlice),rect.left+rect.width/2,rect.bottom+rect.height/2,'normal',Options.SliceAverageTextFontSize);
    else
        DrawRectangle(rect,clr);    
        WriteStringWithCenterPoint(str,rect.left+rect.width/2,rect.bottom+rect.height/2);    
        rect.left   = rect.left - 0.1; rect.width  = 0.1;
        WriteStringWithCenterPoint(SlicesLabels(nSlice),rect.left+rect.width/2,rect.bottom+rect.height/2);
    end
end

if Options.ShowGlobalAverage %center circle, global mean value
    value = BullsEyesInfo.GlobalMean;
    str = sprintf(getFormatStr(value,1),value);        
    if Options.WriteValue
        if Options.WriteMeanTitle
            WriteStringWithCenterPoint('mean',0,0.1,'light',Options.BullseyeTextFontSize,Options.TextColor); 
            WriteStringWithCenterPoint(str,0,-0.05,'bold',Options.BullseyeTextFontSize,Options.TextColor); 
        else
            WriteStringWithCenterPoint(str,0,0,'bold',20,Options.TextColor); 
        end
    end
end


if Options.ShowNormalValue
    FullRadius=0.99;
    rect.width  = 0.35;
    rect.height = 0.15;
    rect.left   = -FullRadius;
    rect.bottom = -FullRadius;
    value = BullsEyesInfo.HealthyValue; 
    clr = getClr(value, BullsEyesInfo.DataRange, BullsEyesInfo.ColorMap);
    str = sprintf(getFormatStr(value,1),value);
    DrawRectangle(rect,clr);
    WriteStringWithCenterPoint(str,rect.left+rect.width/2,rect.bottom+rect.height/2,'bold',14);
    rect.bottom = -FullRadius + 0.15;
    WriteStringWithCenterPoint('Normal',rect.left+rect.width/2,rect.bottom+rect.height/2,'bold',12,Options.TextColor);
end

if Options.ShowColorbar
    cb = colorbar();
    caxis(BullsEyesInfo.DataRange);
    colormap(BullsEyesInfo.ColorMap);
    %cb.Ticks = (0:(1/3):1).*max(BullsEyesInfo.DataRange);
    %cb.FontSize = 25;
    %cb.Label.String = '[ms]';
    %cb.Label.FontName = 25;
    %cb.Label.FontSize = 25;
    %cb.LineWidth = 2;
    %cb.TickDirection = 'both';
    %cb.TickLength = 0.01;
    %cb.Label.Position(1) = 2.5;
end

set(f, 'GraphicsSmoothing', 'on');
set(f, 'Position', [106,42,998,954]);
set(hFigure, 'FontName', 'Bahnschrift');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xData,yData] = DrawOneSegment(clr,nSlice,nSegment,MaxSegment,FullRadius,rStart,fillColor,drawEdges, lineWidth)

segmentAngle = 360/MaxSegment;

rRatioFrom = FullRadius - (rStart .* nSlice);
rRatioTo = FullRadius + rStart - (rStart .* nSlice);

angleFrom = (MaxSegment-nSegment+1)*segmentAngle - segmentAngle/2;
angleTo = angleFrom + segmentAngle;

[xData,yData] = GetSegmentPoints(rRatioFrom,rRatioTo,angleFrom,angleTo);

% if drawEdges, patch(xData, yData, clr, 'LineWidth', lineWidth); end        %
if fillColor && MaxSegment>1, fill(xData,yData,clr,'EdgeColor','none'); end %'LineSmoothing','on'
if drawEdges, plot(xData,yData,'k','LineWidth',lineWidth, 'AlignVertexCenters', 'off'); end        %

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xData,yData] = GetSegmentPoints(rRatioFrom,rRatioTo,angleFrom,angleTo)

Width = 1;
Height= 1;
AngleResolution = 5;
counter = 0;

for theta=angleTo:-AngleResolution:angleFrom
    counter = counter + 1;
    xData(counter) = Width .* rRatioTo .* sind(theta);
    yData(counter) = Height.* rRatioTo .* cosd(theta);
end

if mod(angleFrom,360)==mod(angleTo,360), return; end

for theta=angleFrom:AngleResolution:angleTo
    counter = counter + 1;
    xData(counter) = Width .* rRatioFrom .* sind(theta);
    yData(counter) = Height.* rRatioFrom .* cosd(theta);
end

counter = counter + 1;
xData(counter) = xData(1);
yData(counter) = yData(1);       

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function FormatStr = getFormatStr(value,MaxSegment)
FormatStr = '%2.1f';
if value>99 || MaxSegment>6
    FormatStr = '%2.0f';
elseif (value>9 && value<99)
    FormatStr = '%2.1f';    
elseif value<1
    FormatStr = '%2.2f';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function clr = getClr(value, ValuesRange, ColorMap)

dataMin = ValuesRange(1);
dataRange = ValuesRange(2) - ValuesRange(1);
clrIndex = (value-dataMin)/dataRange .* size(ColorMap,1);
clrIndex = max(floor(clrIndex),1);
clrIndex = min(floor(clrIndex),size(ColorMap,1));
clr = ColorMap(clrIndex,:);  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function DrawRectangle(rect,clr)
xPts = [rect.left,rect.left+rect.width,rect.left+rect.width,rect.left]; 
yPts = [rect.bottom,rect.bottom,rect.bottom+rect.height,rect.bottom+rect.height];
if strcmpi(clr,'none')
    rectangle('Position',[rect.left,rect.bottom,rect.width,rect.height],'LineWidth',2);
else
    patch(xPts,yPts,clr,'LineWidth',2,'LineSmoothing','on');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function WriteStringWithCenterPoint(str,xPos,yPos,FontWeight,FontSize,clr)
h = text(xPos,yPos,1,str);
set(h,'VerticalAlignment','middle');
set(h,'HorizontalAlignment','center');
set(h,'FontName', 'Bahnschrift');
if exist('FontWeight','var') && exist('FontSize','var')
    set(h,'FontSize',FontSize);
    set(h,'FontWeight',FontWeight);   
end
if exist('clr','var')
    set(h,'Color',clr);
end
%set(h,'BackgroundColor',[.7 .9 .7]);
