function saveMatlabFigure(fh, filename, outputDir, BackgroundColor, extensionsStr, Resolution)
% Save current MATLAB figure as a file
% Joao Tourais
% November 2021
%
% Usage:
% fh = figure();
% imagesc(rand(3));
% saveMatlabFigure(fh, 'my_figurename', 'D:\', 'none', {'jpg'; 'png'})

% Default values
switch nargin
    case 2
        outputDir = pwd;
        BackgroundColor = 'none';
        extensionsStr = {'jpg'; 'png'; 'tif'; 'pdf'; 'emf'; 'eps'; 'svg'}; % Extensions of the outputed files
        Resolution = 300;
    case 3
        BackgroundColor = 'none';
        extensionsStr = {'jpg'; 'png'; 'tif'; 'pdf'; 'emf'; 'eps'; 'svg'}; % Extensions of the outputed files
        Resolution = 300;
    case 4
        extensionsStr = {'jpg'; 'png'; 'tif'; 'pdf'; 'emf'; 'eps'; 'svg'}; % Extensions of the outputed files
        Resolution = 300;
    case 5
        Resolution = 300;
    otherwise
        if nargin > 6
            error('Too many inputs!');
        elseif nargin < 2
            error('Not enough inputs (min = 2)!');
        end
end

% If directory do not exists, create it
if ~exist(outputDir, 'dir')
    mkdir(outputDir)
end

fh.WindowState = 'maximized';
fh.Renderer = 'painters';

for ee = 1:size(extensionsStr,1)
    % Filename with extension
    filename = strrep(filename,'@','_');
    filename_e{ee} = sprintf('%s.%s', filename, extensionsStr{ee});
    cGCF = gcf;

    % With exportgraphics the resulting graphic is tightly cropped to a thin margin surrounding your content. (no white box surrounding the image!)
    % However, exportgraphics only supports these extensions
    if (strcmp(extensionsStr{ee},'jpg') || strcmp(extensionsStr{ee},'jpeg') || strcmp(extensionsStr{ee},'png') || strcmp(extensionsStr{ee},'tif') || strcmp(extensionsStr{ee},'tiff') || strcmp(extensionsStr{ee},'pdf') || strcmp(extensionsStr{ee},'emf') || strcmp(extensionsStr{ee},'eps'))
        exportgraphics(cGCF,fullfile(outputDir, filename_e{ee}),'BackgroundColor',BackgroundColor,'ContentType','vector','Resolution',Resolution);
    else
        % If the extension is not supported by exportgraphics (like svg)
        % use the function saveas
        current_dir = pwd;
        cd(outputDir);
        saveas(cGCF,filename_e{ee});
        cd(current_dir)
    end

    clear filename_e;
end
close(fh);
end

