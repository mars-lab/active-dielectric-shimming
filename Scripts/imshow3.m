function imshow3(img, range, shape)
% imshow3(img, [ range, [shape)
%
% Function to display a series of images as a montage.
%
% img - a 3D array representing a series of images
% range - window level (similarly to imshow)
% shape - a 2x1 vector representing the shape of the montage
%
% Example:
%     im = repmat(phantom(128), [1, 1, 6]);
%     figure;
%     imshow3(im, [], [2, 3]);
%
% (c) Michael Lustig 2012
% This MATLAB script defines a function called imshow3 that displays a series of images as a montage. The function takes three input arguments: img, range, and shape.
% The img argument is a 3D array representing a series of images. The dimensions of img are sx, sy, and nc, where sx and sy are the spatial dimensions of each image, and nc is the number of images in the series.
% The range argument is an optional parameter that specifies the window level for displaying the images. If not provided, the function calculates the minimum and maximum values in img and uses them as the default range.
% The shape argument is also optional and represents the desired shape of the montage. It is a 2x1 vector specifying the number of rows and columns in the montage. If not provided, the function calculates the shape based on the number of images in the series.
% The function reshapes the img array and rearranges its dimensions to create the montage. If shape is not provided, the function arranges the images in a square-shaped montage. If shape is provided, the function arranges the images according to the specified shape.
% Finally, the function displays the resulting montage using the imshow function with the specified range.
% The code also includes an example that demonstrates how to use the imshow3 function to display a series of images.

[sx, sy, nc] = size(img);

% Set default range if not provided
if nargin < 2
    range = [min(img(:)), max(img(:))];
end

% Set default range if empty
if isempty(range) == 1
    range = [min(img(:)), max(img(:))];
end

% Set default shape if not provided
if nargin < 3
    if ceil(sqrt(nc))^2 ~= nc
        nc = ceil(sqrt(nc))^2;
        img(end, end, nc) = 0;
    end

    img = reshape(img, sx, sy * nc);
    img = permute(img, [2, 3, 1]);
    img = reshape(img, sy * sqrt(nc), sqrt(nc), sx);
    img = permute(img, [3, 2, 1]);
    img = reshape(img, sx * sqrt(nc), sy * sqrt(nc));
else
    img = reshape(img, sx, sy * nc);
    img = permute(img, [2, 3, 1]);
    img = reshape(img, sy * shape(2), shape(1), sx);
    img = permute(img, [3, 2, 1]);
    img = reshape(img, sx * shape(1), sy * shape(2));
end

% Display the image with the specified range
% imagesc(img, range); colormap(gray(256)); axis('equal');
imshow(img, range);