clear all;
close all;

% Change to path with utility scripts
addpath('F:\Active Dielectric Shimming\Scripts\');

%% Wire length

% Load B1+ maps
for n = 4:15
    B1_C3_0V(:,:,n-3) = load(sprintf('B1_C3_%dcm_0V',n)).B1map;
    B1_C3_5V(:,:,n-3) = load(sprintf('B1_C3_%dcm_5V',n)).B1map;
end

% Mask
i = (0.5:383.5)-192;
j = (0.5:383.5)-192;
[i, j] = meshgrid(i,j);
r = sqrt(i.*i+j.*j);
mask = double(r<152);

% Plot cylindrical phantom wire lengths
f = figure(4);
f.Position = [50 50 1000 1000];
t = tiledlayout(3,4);
t.TileSpacing = 'none';
t.Padding = 'normal';

% Compute B1+ modulation
difference_maps = B1_C3_5V - B1_C3_0V;

for n = 4:15
    plot_B1_map_tile(difference_maps(:,:,n-3), -10, 10, mask,true,true,-100,-100);
end

set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'\DeltaB_{1}^{+} (%)');

function plot_B1_map_tile(data, V_min, V_max, mask, is_difference, display_cask, xc, yc)
    ax = 35;
    ay = 35;
    bx = 350;
    by = 350;
    data = data(ay:by,ax:bx);
    
    mask = mask(ay:by,ax:bx);
    nexttile;
    imagesc(data,'AlphaData',mask);
    
    set(gca,'color',0.92*[1 1 1]);
    if (is_difference==1)
        cm = cbrewer('div','RdBu',100,'spline');
        cm(cm<0) = 0;
        cm(cm>1) = 1;
        cm = flipud(cm);
        colormap(cm);
    else
        colormap jet;
    end

    caxis([V_min V_max]);
    daspect([1 1 1]);
    ax = gca;
    ax.XColor = 'none';
    ax.YColor = 'none';
    set(gcf,'color','w');
    
    if (display_cask)
        % Matlab properties
        hold on;
        xlims = xlim;
        ylims = ylim;

        % Cask/imaging parameters
        FOV = 450;
        N = 384;
        num_pockets = 3;
        pocket_width = 50;
        pocket_separation = 2;

        % Compute reference points/distances
        voxel_size = FOV/N;
        cask_size = num_pockets*pocket_width+(num_pockets+1)*pocket_separation;
        cask_size_px = cask_size/voxel_size;
        x0 = (xlims(1)+xlims(2))*0.5-cask_size_px/2;
        y0 = (ylims(1)+ylims(2))*0.5-cask_size_px/2;
        dx = cask_size_px/num_pockets;
        dy = dx;

        for n = 1:num_pockets+1
            x1 = x0;
            x2 = x0 + dx*num_pockets;
            y1 = y0+(n-1)*dy;
            y2 = y1;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end

        for n = 1:num_pockets+1
            x1 = x0+(n-1)*dx;
            x2 = x1;
            y1 = y0;
            y2 = y0 + dy*num_pockets;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end
    end
    
    % Draw ROI
    circle(xc, yc,10);
end

function [p, ave, ci, SD] = get_stats(B1_C1_5V, B1_C1_0V, ic, jc)
    skip = 34;
    a = 1+skip;
    b = 384-skip;
    B1_C1_5V = B1_C1_5V(a:b,a:b,:);
    B1_C1_0V = B1_C1_0V(a:b,a:b,:);

    r = 10;
    j = 1:size(B1_C1_5V,1);
    i = 1:size(B1_C1_5V,2);
    k = 1:size(B1_C1_5V,3);
    [I, J, K] = meshgrid(i,j,k);
    d = sqrt((I-ic).*(I-ic)+(J-jc).*(J-jc));
    
    xx = B1_C1_5V(d<r);
    yy = B1_C1_0V(d<r);
    [h, p, ci, stats] = ttest(xx(:),yy(:));
    SD = stats.sd;
    ave = mean(xx(:)-yy(:));
end

function h = circle(x,y,r)
    hold on
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    h = plot(xunit, yunit,'r','LineWidth',1.25);
    hold off
end
