clear all;
close all;

% Change to path with utility scripts
addpath('F:\Active Dielectric Shimming\Scripts\');

%% Gel torso phantom 4 configs

% Load B1+ maps
for i = 1:10
    B1_C1_0V(:,:,i) = load(sprintf('B1_C1_0V_N%d',i)).B1map;
    B1_C2_0V(:,:,i) = load(sprintf('B1_C2_0V_N%d',i)).B1map;
    B1_C3_0V(:,:,i) = load(sprintf('B1_C3_0V_N%d',i)).B1map;
    B1_C4_0V(:,:,i) = load(sprintf('B1_C4_0V_N%d',i)).B1map;
    
    B1_C1_5V(:,:,i) = load(sprintf('B1_C1_5V_N%d',i)).B1map;
    B1_C2_5V(:,:,i) = load(sprintf('B1_C2_5V_N%d',i)).B1map;
    B1_C3_5V(:,:,i) = load(sprintf('B1_C3_5V_N%d',i)).B1map;
    B1_C4_5V(:,:,i) = load(sprintf('B1_C4_5V_N%d',i)).B1map;
end

% Compute mean
N = 10;
mean_B1_C1_0V = mean(B1_C1_0V(:,:,1:N),3);
mean_B1_C2_0V = mean(B1_C2_0V(:,:,1:N),3);
mean_B1_C3_0V = mean(B1_C3_0V(:,:,1:N),3);
mean_B1_C4_0V = mean(B1_C4_0V(:,:,1:N),3);

mean_B1_C1_5V = mean(B1_C1_5V(:,:,1:N),3);
mean_B1_C2_5V = mean(B1_C2_5V(:,:,1:N),3);
mean_B1_C3_5V = mean(B1_C3_5V(:,:,1:N),3);
mean_B1_C4_5V = mean(B1_C4_5V(:,:,1:N),3);

% Compute B1+ modulation
difference_maps(:,:,1) = mean_B1_C1_5V - mean_B1_C1_0V;
difference_maps(:,:,2) = mean_B1_C2_5V - mean_B1_C2_0V;
difference_maps(:,:,3) = mean_B1_C3_5V - mean_B1_C3_0V;
difference_maps(:,:,4) = mean_B1_C4_5V - mean_B1_C4_0V;

% Create mask
mask(:,:,1) = mean_B1_C1_0V > 28;
mask(:,:,2) = mean_B1_C2_0V > 28;
mask(:,:,3) = mean_B1_C3_0V > 28;
mask(:,:,4) = mean_B1_C4_0V > 28;

% Plot 4 configs
f = figure(1);
f.Position = [50 50 800 800];
t = tiledlayout(2,2);
t.TileSpacing = 'none';
t.Padding = 'normal';

plot_B1_map_tile(difference_maps(:,:,1), -10, 10, mask(:,:,1), true, true, 113, 136);
plot_B1_map_tile(difference_maps(:,:,2), -10, 10, mask(:,:,2), true, true, 176, 130);
plot_B1_map_tile(difference_maps(:,:,3), -10, 10, mask(:,:,3), true, true, 111, 181);
plot_B1_map_tile(difference_maps(:,:,4), -10, 10, mask(:,:,4), true, true, 186, 186);

set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'\DeltaB_{1}^{+} (%)');

% Compute mean modulation in ROI
[p1, ave1, ci1, SD1] = get_stats(B1_C1_5V,B1_C1_0V,113,136);
[p2, ave2, ci2, SD2] = get_stats(B1_C2_5V,B1_C2_0V,176,130);
[p3, ave3, ci3, SD3] = get_stats(B1_C3_5V,B1_C3_0V,111,181);
[p4, ave4, ci4, SD4] = get_stats(B1_C4_5V,B1_C4_0V,186,186);

% Correlation
i_start = 91;
j_start = 91;
stepsize = 45;

X1 = i_start:i_start+2*stepsize;
X2 = i_start+stepsize:i_start+3*stepsize;
Y1 = j_start:j_start+2*stepsize;
Y2 = j_start+stepsize:j_start+3*stepsize;

ax = 20;
ay = 10;
bx = 335;
by = 325;
difference_maps = difference_maps(ay:by,ax:bx,:); % crop image

C1 = difference_maps(Y1,X1,1);
C2 = difference_maps(Y1,X2,2);
C3 = difference_maps(Y2,X1,3);
C4 = difference_maps(Y2,X2,4);

C1N = C1./sqrt(sum(sum(C1.*C1)));
C2N = C2./sqrt(sum(sum(C2.*C2)));
C3N = C3./sqrt(sum(sum(C3.*C3)));
C4N = C4./sqrt(sum(sum(C4.*C4)));

C12 = sum(sum(C1N.*C2N));
C13 = sum(sum(C1N.*C3N));
C14 = sum(sum(C1N.*C4N));

%% Gel torso phantom depth slices

% Slice depth (note: slice thickness is 10 mm and the number here is the
% position at the topside of the slice, so e.g. 5 mm means from 5 to 15 mm)
depths = [0 5 10 15 20 25 30 40 50 60 70 80 90];

% Load B1+ maps
for k = 1:length(depths)
    for i = 1:3
        B1_C2_0V_depths(:,:,i,k) = load(sprintf('B1_C2_0V_D%d_N%d',depths(k),i)).B1map;
        B1_C2_5V_depths(:,:,i,k) = load(sprintf('B1_C2_5V_D%d_N%d',depths(k),i)).B1map;
    end
end

% Compute mean
mean_B1_C2_0V_depths = mean(B1_C2_0V_depths,3);
mean_B1_C2_5V_depths = mean(B1_C2_5V_depths,3);

% Compute mean B1+ modulation in ROI
for k = 1:length(depths)    
    [p5(k), ave5(k), ci5(:,k), SD5(k)] = get_stats(B1_C2_5V_depths(:,:,:,k),B1_C2_0V_depths(:,:,:,k),176,130);
end

% Plot a few selected slices (B1+ modulation)
f = figure(2);
f.Position = [50 50 1800 600];
t = tiledlayout(1,4);
t.TileSpacing = 'none';
t.Padding = 'normal';

slices = [3 6 8 10];
for k = 1:length(slices)
    % Mask
    mask = mean_B1_C2_0V_depths(:,:,1,slices(k)) > 28;
    % B1+ modulation
    data = mean_B1_C2_5V_depths(:,:,1,slices(k)) - mean_B1_C2_0V_depths(:,:,1,slices(k));
    % Plot
    plot_B1_map_tile(data, -10, 10, mask, true, true, 176, 130);
end

set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
title(cb,'\DeltaB_{1}^{+} (%)');
cb.FontSize = 16;

% Plot a few selected slices (B1+ maps)
f = figure(3);
f.Position = [50 50 1800 600];
t = tiledlayout(1,4);
t.TileSpacing = 'none';
t.Padding = 'normal';

slices = [3 3 3 3];
for k = 1:length(slices)
    % Mask
    mask = mean_B1_C2_0V_depths(:,:,1,slices(k)) > 25;
    % B1+ map
    data = mean_B1_C2_5V_depths(:,:,1,slices(k));
    %Plot
    plot_B1_map_tile(data, 60, 105, mask, false, true, 176, 130);
end
set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
title(cb,'\DeltaB_{1}^{+} (%)');
cb.FontSize = 16;

% Figure with depth dependence
a = 32.1; % mean parasternal skin-heart distance
b = 7.9; % SD of parasternal skin-heart distance
c1 = a-b;
c2 = a+b;

f = figure(4);
f.Position = [50 50 800 400];
clear p;
p(1) = fill([a-b a+b a+b a-b],[-30 -30 0 0],[0.84 0.84 0.84],'EdgeColor','none');
hold on;
plot([a a],[-30 0],'Color','k');
p(2) = errorbar(depths+5,ave5,ci5(1,:)-ave5,ave5-ci5(2,:),'k.','MarkerSize',15,'CapSize',1);
grid on
grid minor
set(gca, 'FontName', 'Inter');
legend(p,'Parasternal skin-heart distance','Modulation in ROI','Location','SouthEast');
xlabel('Depth (mm)','FontSize',16);
ylabel('\DeltaB_{1}^{+} (%)','FontSize',16);
ax = gca;
ax.FontSize = 16;




function plot_B1_map_tile(data, V_min, V_max, mask, is_difference, display_cask, xc, yc)
    ax = 20;
    ay = 10;
    bx = 335;
    by = 325;
    data = data(ay:by,ax:bx);
    mask = mask(ay:by,ax:bx);
    nexttile;
    imagesc(data,'AlphaData',mask);
    
    set(gca,'color',0.92*[1 1 1]);
    if (is_difference==1)
        cm = cbrewer('div','RdBu',100,'spline');
        cm(cm<0) = 0;
        cm(cm>1) = 1;
        cm = flipud(cm);
        colormap(cm);
    else
        colormap jet;
    end

    caxis([V_min V_max]);
    daspect([1 1 1]);
    ax = gca;
    ax.XColor = 'none';
    ax.YColor = 'none';
    set(gcf,'color','w');
    
    if (display_cask)
        % Matlab properties
        hold on;
        xlims = xlim;
        ylims = ylim;

        % Cask/imaging parameters
        FOV = 450;
        N = 384;
        num_pockets = 3;
        pocket_width = 50;
        pocket_separation = 2;

        % Compute reference points/distances
        voxel_size = FOV/N;
        cask_size = num_pockets*pocket_width+(num_pockets+1)*pocket_separation;
        cask_size_px = cask_size/voxel_size;
        x0 = (xlims(1)+xlims(2))*0.5-cask_size_px/2;
        y0 = (ylims(1)+ylims(2))*0.5-cask_size_px/2;
        dx = cask_size_px/num_pockets;
        dy = dx;

        for n = 1:num_pockets+1
            x1 = x0;
            x2 = x0 + dx*num_pockets;
            y1 = y0+(n-1)*dy;
            y2 = y1;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.5);
        end

        for n = 1:num_pockets+1
            x1 = x0+(n-1)*dx;
            x2 = x1;
            y1 = y0;
            y2 = y0 + dy*num_pockets;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.5);
        end
    end
    
    % Draw ROI
    circle(xc, yc,10);
end

function [p, ave, ci, SD] = get_stats(B1_C1_5V, B1_C1_0V, ic, jc)
    ax = 20;
    ay = 10;
    bx = 335;
    by = 325;
    B1_C1_5V = B1_C1_5V(ay:by,ax:bx);
    B1_C1_0V = B1_C1_0V(ay:by,ax:bx);

    r = 10;
    j = 1:size(B1_C1_5V,1);
    i = 1:size(B1_C1_5V,2);
    k = 1:size(B1_C1_5V,3);
    [I, J, K] = meshgrid(i,j,k);
    d = sqrt((I-ic).*(I-ic)+(J-jc).*(J-jc));
    
    xx = B1_C1_5V(d<r);
    yy = B1_C1_0V(d<r);
    [h, p, ci, stats] = ttest(xx(:),yy(:));
    SD = stats.sd;
    ave = mean(xx(:)-yy(:));
end

function h = circle(x,y,r)
    hold on
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    h = plot(xunit, yunit,'r','LineWidth',1.5);
    hold off
end
