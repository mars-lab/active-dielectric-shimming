clear all;
close all;

% Change to path with utility scripts
addpath('F:\Active Dielectric Shimming\Scripts\');

%% Cylindrical phantom activation function

% Bias voltages
mV = [-5000 -4000 -3000 -2500 -2000 -1500 -1000 -800 -600 -500 -400 -300 -200 -100 0 50 100 120 140 ...
    160 180 190 200 205 210 220 240 260 280 300 350 400 500 600 800 1000 1500 2000 2500 3000 4000 5000];

% Load B1+ maps
for k = 1:length(mV)
    B1_C3(:,:,k) = load(sprintf('B1_C3_%dmV',mV(k))).B1map;
end

% Mask
i = (0.5:383.5)-192;
j = (0.5:383.5)-192;
[i, j] = meshgrid(i,j);
r = sqrt(i.*i+j.*j);
mask = double(r<152);

% ROI
xc = 90;
yc = 170;

% Plot B1+ difference maps
f = figure(1);
f.Position = [50 50 1500 900];
t = tiledlayout(5,9);
t.TileSpacing = 'none';
t.Padding = 'normal';

for k = 1:length(mV)
    data = B1_C3(:,:,k) - B1_C3(:,:,15);
    plot_B1_map_tile(data, -10, 10, mask, true, true, xc, yc);
    [p(k), ave(k), ci(:,k), SD(k)] = get_stats2(B1_C3(:,:,k),xc,yc);
end
set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'\DeltaB_{1}^{+} (%)');

% Make graph
f = figure(2);
f.Position = [50 50 1250 600];
scatter(mV*1e-3,ave,100,'k.');
hold on
eb = errorbar(mV*1e-3,ave,ave-ci(1,:), ci(2,:)-ave,'k.','MarkerSize',15,'CapSize',1);

set(gca, 'FontName', 'Inter');
grid(gca,'minor')
grid on;
xlabel('Bias (V)');
ylabel('B_{1}^{+} (%)');
ax = gca;
ax.FontSize = 20;

% Plot B1+ maps for some slices
f = figure(3);
f.Position = [50 50 1500 500];
t = tiledlayout(1,3);
t.TileSpacing = 'none';
t.Padding = 'normal';

V = [15 25 32];
for k = 1:3
    data = B1_C3(:,:,V(k));
    plot_B1_map_tile(data, -10, 10, mask, true, true, xc, yc);
    
    colormap jet;
    daspect([1,1,1]);
    caxis([60 105]);
    ax = gca;
    ax.XColor = 'none';
    ax.YColor = 'none';
    set(gcf,'color','w');
end

set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'B_{1}^{+} (%)','FontSize',16);

% Plot B1+ difference maps for some slices
f = figure(4);
f.Position = [50 50 1500 500];
t = tiledlayout(1,3);
t.TileSpacing = 'none';
t.Padding = 'normal';

V = [15 25 32];
for k = 1:3
    data = B1_C3(:,:,V(k))-B1_C3(:,:,15);
    plot_B1_map_tile(data, -10, 10, mask,true,true, xc, yc);
end
set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'\DeltaB_{1}^{+} (%)','FontSize',16);

% Average B1+ for 1V - 5V
data1 = mean(B1_C3(:,:,36:42),3);
[p2, ave2, ci2(:), SD2] = get_stats2(data1,xc,yc);

% Average B1+ for -5V - 0V
data2 = mean(B1_C3(:,:,1:15),3);
[p3, ave3, ci3(:), SD3] = get_stats2(data2,xc,yc);

% Average B1+ modulation between 1V - 5V and -5V - 0V
[p4, ave4, ci4(:), SD4] = get_stats(data1,data2,90,170);


function plot_B1_map_tile(data, V_min, V_max, mask, is_difference, display_cask, xc, yc)
    ax = 35;
    ay = 35;
    bx = 350;
    by = 350;
    data = data(ay:by,ax:bx);
    
    mask = mask(ay:by,ax:bx);
    nexttile;
    imagesc(data,'AlphaData',mask);
    
    set(gca,'color',0.92*[1 1 1]);
    if (is_difference==1)
        cm = cbrewer('div','RdBu',100,'spline');
        cm(cm<0) = 0;
        cm(cm>1) = 1;
        cm = flipud(cm);
        colormap(cm);
    else
        colormap jet;
    end

    caxis([V_min V_max]);
    daspect([1 1 1]);
    ax = gca;
    ax.XColor = 'none';
    ax.YColor = 'none';
    set(gcf,'color','w');
    
    if (display_cask)
        % Matlab properties
        hold on;
        xlims = xlim;
        ylims = ylim;

        % Cask/imaging parameters
        FOV = 450;
        N = 384;
        num_pockets = 3;
        pocket_width = 50;
        pocket_separation = 2;

        % Compute reference points/distances
        voxel_size = FOV/N;
        cask_size = num_pockets*pocket_width+(num_pockets+1)*pocket_separation;
        cask_size_px = cask_size/voxel_size;
        x0 = (xlims(1)+xlims(2))*0.5-cask_size_px/2;
        y0 = (ylims(1)+ylims(2))*0.5-cask_size_px/2;
        dx = cask_size_px/num_pockets;
        dy = dx;

        for n = 1:num_pockets+1
            x1 = x0;
            x2 = x0 + dx*num_pockets;
            y1 = y0+(n-1)*dy;
            y2 = y1;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end

        for n = 1:num_pockets+1
            x1 = x0+(n-1)*dx;
            x2 = x1;
            y1 = y0;
            y2 = y0 + dy*num_pockets;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end
    end
    
    % Draw ROI
    circle(xc, yc, 10);
end

function [p, ave, ci, SD] = get_stats(B1_C1_5V, B1_C1_0V, ic, jc)
    skip = 34;
    a = 1+skip;
    b = 384-skip;
    B1_C1_5V = B1_C1_5V(a:b,a:b,:);
    B1_C1_0V = B1_C1_0V(a:b,a:b,:);

    r = 10;
    j = 1:size(B1_C1_5V,1);
    i = 1:size(B1_C1_5V,2);
    k = 1:size(B1_C1_5V,3);
    [I, J, K] = meshgrid(i,j,k);
    d = sqrt((I-ic).*(I-ic)+(J-jc).*(J-jc));
    
    xx = B1_C1_5V(d<r);
    yy = B1_C1_0V(d<r);
    [h, p, ci, stats] = ttest(xx(:),yy(:));
    SD = stats.sd;
    ave = mean(xx(:)-yy(:));
end

function [p, ave, ci, SD] = get_stats2(B1_C1_5V, ic, jc)
    skip = 34;
    a = 1+skip;
    b = 384-skip;
    B1_C1_5V = B1_C1_5V(a:b,a:b,:);

    r = 10;
    j = 1:size(B1_C1_5V,1);
    i = 1:size(B1_C1_5V,2);
    k = 1:size(B1_C1_5V,3);
    [I, J, K] = meshgrid(i,j,k);
    d = sqrt((I-ic).*(I-ic)+(J-jc).*(J-jc));
    
    xx = B1_C1_5V(d<r);
    [h, p, ci, stats] = ttest(xx(:));
    SD = stats.sd;
    ave = mean(xx(:));
end

function h = circle(x,y,r)
    hold on
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    h = plot(xunit, yunit,'r','LineWidth',1.25); % Use 'k' for the B1+ magnitude maps instead of 'r'
    hold off
end
