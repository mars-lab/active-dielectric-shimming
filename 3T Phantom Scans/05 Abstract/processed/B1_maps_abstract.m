clear all;
close all;

% Change to path with utility scripts
addpath('F:\Active Dielectric Shimming\Scripts\');

%% Cylindrical phantom uncoupled vs. no cask (to show effect of cask)

% Load B1+ maps
B1_NC = load('B1_NC').B1map;
B1_UC = load('B1_UC').B1map;

% Create mask
i = (0.5:383.5)-192;
j = (0.5:383.5)-192;
[i, j] = meshgrid(i,j);
r = sqrt(i.*i+j.*j);
mask = double(r<152);

% Plot B1 map without cask
f = figure(1);
f.Position = [50 50 840 800];
t = tiledlayout(1,1);
t.TileSpacing = 'none';
t.Padding = 'normal';

plot_B1_map_tile(B1_NC, 60, 105, mask,false,false,-999,-999);

set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'B_{1}^{+} (%)');

% Plot B1 difference map
f = figure(2);
f.Position = [50 50 840 800];
t = tiledlayout(1,1);
t.TileSpacing = 'none';
t.Padding = 'normal';

plot_B1_map_tile(B1_UC - B1_NC, -10, 10, mask,true,true,-999,-999);

set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'\DeltaB_{1}^{+} (%)');

%% Cylindrical phantom 4 configs - pockets directly connected with 20 cm wires

% Load B1+ maps
B1_UC = load('B1_UC').B1map;
for i = 1:10
    B1_C1_20cm(:,:,i) = load(sprintf('B1_C1_20cm_N%d',i)).B1map;
    B1_C2_20cm(:,:,i) = load(sprintf('B1_C2_20cm_N%d',i)).B1map;
    B1_C3_20cm(:,:,i) = load(sprintf('B1_C3_20cm_N%d',i)).B1map;
    B1_C4_20cm(:,:,i) = load(sprintf('B1_C4_20cm_N%d',i)).B1map;
end

% Compute mean
N = 10;
mean_B1_C1_20cm = mean(B1_C1_20cm(:,:,1:N),3);
mean_B1_C2_20cm = mean(B1_C2_20cm(:,:,1:N),3);
mean_B1_C3_20cm = mean(B1_C3_20cm(:,:,1:N),3);
mean_B1_C4_20cm = mean(B1_C4_20cm(:,:,1:N),3);

% Compute B1+ modulation
difference_maps(:,:,1) = mean_B1_C1_20cm - B1_UC;
difference_maps(:,:,2) = mean_B1_C2_20cm - B1_UC;
difference_maps(:,:,3) = mean_B1_C3_20cm - B1_UC;
difference_maps(:,:,4) = mean_B1_C4_20cm - B1_UC;

% Create mask
i = (0.5:383.5)-192;
j = (0.5:383.5)-192;
[i, j] = meshgrid(i,j);
r = sqrt(i.*i+j.*j);
mask = double(r<152);

% Plot cylindrical phantom 4 configs
f = figure(3);
f.Position = [50 50 800 800];
t = tiledlayout(2,2);
t.TileSpacing = 'none';
t.Padding = 'normal';

plot_B1_map_tile(mean_B1_C1_20cm - B1_UC, -10, 10, mask,true,true,61,140);
plot_B1_map_tile(mean_B1_C2_20cm - B1_UC, -10, 10, mask,true,true,176,125);
plot_B1_map_tile(mean_B1_C3_20cm - B1_UC, -10, 10, mask,true,true,74,183);
plot_B1_map_tile(mean_B1_C4_20cm - B1_UC, -10, 10, mask,true,true,204,232);

set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'\DeltaB_{1}^{+} (%)');

% Compute mean modulation in ROI
[p1, ave1, ci1, SD1] = get_stats(mean_B1_C1_20cm,B1_UC,61,140);
[p2, ave2, ci2, SD2] = get_stats(mean_B1_C2_20cm,B1_UC,176,125);
[p3, ave3, ci3, SD3] = get_stats(mean_B1_C3_20cm,B1_UC,74,183);
[p4, ave4, ci4, SD4] = get_stats(mean_B1_C4_20cm,B1_UC,204,232);

%% Cylindrical phantom 4 configs - pockets connected via switchboard with 10 cm wires

% Load B1+ maps
B1_C1_10cm_0V = load('B1_C1_10cm_0V').B1map;
B1_C2_10cm_0V = load('B1_C2_10cm_0V').B1map;
B1_C3_10cm_0V = load('B1_C3_10cm_0V').B1map;
B1_C4_10cm_0V = load('B1_C4_10cm_0V').B1map;
for i = 1:10
    B1_C1_10cm_5V(:,:,i) = load(sprintf('B1_C1_10cm_5V_N%d',i)).B1map;
    B1_C2_10cm_5V(:,:,i) = load(sprintf('B1_C2_10cm_5V_N%d',i)).B1map;
    B1_C3_10cm_5V(:,:,i) = load(sprintf('B1_C3_10cm_5V_N%d',i)).B1map;
    B1_C4_10cm_5V(:,:,i) = load(sprintf('B1_C4_10cm_5V_N%d',i)).B1map;
end

% Compute mean
N = 10;
mean_B1_C1_10cm_5V = mean(B1_C1_10cm_5V(:,:,1:N),3);
mean_B1_C2_10cm_5V = mean(B1_C2_10cm_5V(:,:,1:N),3);
mean_B1_C3_10cm_5V = mean(B1_C3_10cm_5V(:,:,1:N),3);
mean_B1_C4_10cm_5V = mean(B1_C4_10cm_5V(:,:,1:N),3);

% Compute B1+ modulation
difference_maps(:,:,1) = mean_B1_C1_10cm_5V - B1_C1_10cm_0V;
difference_maps(:,:,2) = mean_B1_C2_10cm_5V - B1_C2_10cm_0V;
difference_maps(:,:,3) = mean_B1_C3_10cm_5V - B1_C3_10cm_0V;
difference_maps(:,:,4) = mean_B1_C4_10cm_5V - B1_C4_10cm_0V;

% Create mask
i = (0.5:383.5)-192;
j = (0.5:383.5)-192;
[i, j] = meshgrid(i,j);
r = sqrt(i.*i+j.*j);
mask = double(r<152);

% Plot cylindrical phantom 4 configs
f = figure(4);
f.Position = [50 50 800 800];
t = tiledlayout(2,2);
t.TileSpacing = 'none';
t.Padding = 'normal';

plot_B1_map_tile(mean_B1_C1_10cm_5V - B1_C1_10cm_0V, -10, 10, mask,true,true,88,128);
plot_B1_map_tile(mean_B1_C2_10cm_5V - B1_C2_10cm_0V, -10, 10, mask,true,true,153,114);
plot_B1_map_tile(mean_B1_C3_10cm_5V - B1_C3_10cm_0V, -10, 10, mask,true,true,92,169);
plot_B1_map_tile(mean_B1_C4_10cm_5V - B1_C4_10cm_0V, -10, 10, mask,true,true,220,221);

set(gca, 'FontName', 'Inter');
cb = colorbar;
cb.Layout.Tile = 'east';
cb.FontSize = 16;
title(cb,'\DeltaB_{1}^{+} (%)');

% Compute mean modulation in ROI
[p5, ave5, ci5, SD5] = get_stats(mean_B1_C1_10cm_5V,B1_C1_10cm_0V,88,128);
[p6, ave6, ci6, SD6] = get_stats(mean_B1_C2_10cm_5V,B1_C2_10cm_0V,153,114);
[p7, ave7, ci7, SD7] = get_stats(mean_B1_C3_10cm_5V,B1_C3_10cm_0V,92,169);
[p8, ave8, ci8, SD8] = get_stats(mean_B1_C4_10cm_5V,B1_C4_10cm_0V,220,221);

function plot_B1_map_tile(data, V_min, V_max, mask, is_difference, display_cask, xc, yc)
    ax = 35;
    ay = 35;
    bx = 350;
    by = 350;
    data = data(ay:by,ax:bx);
    
    mask = mask(ay:by,ax:bx);
    nexttile;
    imagesc(data,'AlphaData',mask);
    
    set(gca,'color',0.92*[1 1 1]);
    if (is_difference==1)
        cm = cbrewer('div','RdBu',100,'spline');
        cm(cm<0) = 0;
        cm(cm>1) = 1;
        cm = flipud(cm);
        colormap(cm);
    else
        colormap jet;
    end

    caxis([V_min V_max]);
    daspect([1 1 1]);
    ax = gca;
    ax.XColor = 'none';
    ax.YColor = 'none';
    set(gcf,'color','w');
    
    if (display_cask)
        % Matlab properties
        hold on;
        xlims = xlim;
        ylims = ylim;

        % Cask/imaging parameters
        FOV = 450;
        N = 384;
        num_pockets = 3;
        pocket_width = 50;
        pocket_separation = 2;

        % Compute reference points/distances
        voxel_size = FOV/N;
        cask_size = num_pockets*pocket_width+(num_pockets+1)*pocket_separation;
        cask_size_px = cask_size/voxel_size;
        x0 = (xlims(1)+xlims(2))*0.5-cask_size_px/2;
        y0 = (ylims(1)+ylims(2))*0.5-cask_size_px/2;
        dx = cask_size_px/num_pockets;
        dy = dx;

        for n = 1:num_pockets+1
            x1 = x0;
            x2 = x0 + dx*num_pockets;
            y1 = y0+(n-1)*dy;
            y2 = y1;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end

        for n = 1:num_pockets+1
            x1 = x0+(n-1)*dx;
            x2 = x1;
            y1 = y0;
            y2 = y0 + dy*num_pockets;
            plot([x1 x2], [y1 y2],'k','LineWidth',1.25);
        end
    end
    
    % Draw ROI
    circle(xc, yc,10);
end

function [p, ave, ci, SD] = get_stats(B1_C1_5V, B1_C1_0V, ic, jc)
    skip = 34;
    a = 1+skip;
    b = 384-skip;
    B1_C1_5V = B1_C1_5V(a:b,a:b,:);
    B1_C1_0V = B1_C1_0V(a:b,a:b,:);

    r = 10;
    j = 1:size(B1_C1_5V,1);
    i = 1:size(B1_C1_5V,2);
    k = 1:size(B1_C1_5V,3);
    [I, J, K] = meshgrid(i,j,k);
    d = sqrt((I-ic).*(I-ic)+(J-jc).*(J-jc));
    
    xx = B1_C1_5V(d<r);
    yy = B1_C1_0V(d<r);
    [h, p, ci, stats] = ttest(xx(:),yy(:));
    SD = stats.sd;
    ave = mean(xx(:)-yy(:));
end

function h = circle(x,y,r)
    hold on
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + x;
    yunit = r * sin(th) + y;
    h = plot(xunit, yunit,'r','LineWidth',1.25);
    hold off
end
