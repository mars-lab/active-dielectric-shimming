**Project**

Welcome to the Active Dielectric Shimming project!

The main goal of this project is to develop a device for flexible B1+ field homogenization. The device consists of an array of small dielectric pockets with remotely switchable connections. This is done by inserting electrodes into the pockets and then connecting a subset of pockets to a switchboard on which PIN-diodes are located. These PIN-diodes act as variable resistors for radiofrequency (RF) signals by application of a bias voltage. The subset of dielectric pockets that is coupled using a forward bias voltage is expected to function as a single large dielectric pad, where the combined effect is much larger than the sum of the contributions of each pocket separately.

**Contents**

The directory "3T Phantom Scans" contains scan data (directory "raw") and extracted B1+ maps (directory "processed") with a script to visualize the results. The data is compressed into .zip files. A text file describes the experiments that were performed. One or more images with results are included for each set of experiments. A manual is included with instructions on how the build the active dielectric shimming array. Rhino 3D models and 3D objects of the shimming array are included as well.

The directory "CST Simulations" contains CST Studio Suite 2023 model files (.cst) and a volume extract of the resulting B1+ field (.h5) for each simulation. Each series of simulations is described using a .txt file. The .h5 files are compressed into the file "results.zip". A Matlab script is provided to visualize the results (i.e. B1+ magnitude or B1+ difference map). The Matlab script is accompanied with an image showing the results from the script. The .h5 files in the .zip should be decompressed before running the Matlab script. A manual is included with instructions on how to get started with the .cst files.

The directory "Scripts" contains auxiliary Matlab scripts that are used for plotting the experimental/simulated results.